# Indonesian translations for kcm_autostart package.
# Copyright (C) 2010 This_file_is_part_of_KDE
# This file is distributed under the same license as the kcm_autostart package.
#
# Andhika Padmawan <andhika.padmawan@gmail.com>, 2010-2014.
# Wantoyo <wantoyek@gmail.com>, 2017, 2018, 2019.
# Linerly <linerly@protonmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-26 01:47+0000\n"
"PO-Revision-Date: 2022-03-03 12:50+0700\n"
"Last-Translator: Linerly <linerly@protonmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: autostartmodel.cpp:374
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "\"%1\" bukan alur mutlak."

#: autostartmodel.cpp:377
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1\" tidak ada."

#: autostartmodel.cpp:380
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" bukan sebuah file."

#: autostartmodel.cpp:383
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\" bukan dapat dibaca."

#: ui/entry.qml:52
#, fuzzy, kde-format
#| msgid "Name"
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Nama"

#: ui/entry.qml:57
#, fuzzy, kde-format
#| msgid "Status"
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Status"

#: ui/entry.qml:62
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr ""

#: ui/entry.qml:71
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr ""

#: ui/entry.qml:71
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "Pemulaian"

#: ui/entry.qml:105
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr ""

#: ui/entry.qml:109
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr ""

#: ui/main.qml:42
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:56
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:58
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:69
#, fuzzy, kde-format
#| msgid "Add…"
msgctxt "@action:button"
msgid "Add…"
msgstr "Tambahkan..."

#: ui/main.qml:80
#, kde-format
msgid "Add Application…"
msgstr "Tambahkan Aplikasi..."

#: ui/main.qml:85
#, kde-format
msgid "Add Login Script…"
msgstr "Tambah Skrip Masuk..."

#: ui/main.qml:90
#, kde-format
msgid "Add Logout Script…"
msgstr "Tambah Skrip Keluar..."

#: ui/main.qml:130
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:154
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr ""

#: ui/main.qml:159 unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr ""

#: ui/main.qml:165
#, fuzzy, kde-format
#| msgid "Properties"
msgctxt "@action:button"
msgid "See properties"
msgstr "&Properti..."

#: ui/main.qml:171
#, fuzzy, kde-format
#| msgid "Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "&Hapus"

#: ui/main.qml:181
#, kde-format
msgid "Applications"
msgstr "Aplikasi"

#: ui/main.qml:184
#, kde-format
msgid "Login Scripts"
msgstr "Skrip Masuk"

#: ui/main.qml:187
#, kde-format
msgid "Pre-startup Scripts"
msgstr "Skip Prahidup"

#: ui/main.qml:190
#, kde-format
msgid "Logout Scripts"
msgstr "Skrip Keluar"

#: ui/main.qml:198
#, kde-format
msgid "No user-specified autostart items"
msgstr "Tidak ada item pemulaian otomatis yang dispesifikasikan oleh pengguna"

#: ui/main.qml:199
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""
"Klik tombol <interface>Tambahkan...</interface> untuk menambahkan beberapa "
"hal"

#: ui/main.qml:214
#, kde-format
msgid "Choose Login Script"
msgstr "Pilih Skrip Masuk"

#: ui/main.qml:234
#, kde-format
msgid "Choose Logout Script"
msgstr "Pilih Skip Keluar"

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr ""

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr ""

#: unit.cpp:28
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "Pemulaian"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr ""

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:153
#, kde-format
msgid "Failed to open journal"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Andhika Padmawan,Wantoyo"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "andhika.padmawan@gmail.com,wantoyek@gmail.com"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Modul Panel Kendali Pengelola Automulai Sesi"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Hak Cipta © 2006–2010 Tim Pengelola Automulai"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Pemelihara"

#, fuzzy
#~| msgid "Advanced..."
#~ msgid "Add..."
#~ msgstr "Tingkat lanjut..."

#~ msgid "Shell script path:"
#~ msgstr "Alur skrip shell:"

#~ msgid "Create as symlink"
#~ msgstr "Ciptakan sebagai tautan simbolis"

#~ msgid "Autostart only in Plasma"
#~ msgstr "Automulai hanya di Plasma"

#~ msgid "Command"
#~ msgstr "Perintah"

#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Jalankan Pada Saat"

#~ msgid "Session Autostart Manager"
#~ msgstr "Pengelola Automulai KDE"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Difungsikan"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Dinonfungsikan"

#~ msgid "Desktop File"
#~ msgstr "File Desktop"

#~ msgid "Script File"
#~ msgstr "File Skrip"

#~ msgid "Add Program..."
#~ msgstr "Tambah Program..."

#~ msgid "Before session startup"
#~ msgstr "Sebelum pemulaian sesi"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "Hanya file dengan ekstensi “.sh” yang diizinkan mengatur lingkungan."

#~ msgid "Shutdown"
#~ msgstr "Matikan"
