# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: systemmonitor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-03 00:46+0000\n"
"PO-Revision-Date: 2023-10-04 15:47+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.3.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "צוות התרגום של KDE ישראל"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-l10n-he@kde.org"

#: kdedksysguard.cpp:40
#, kde-format
msgid "Show System Activity"
msgstr "הצגת פעילות מערכת"

#: main.cpp:23
#, kde-format
msgid "System Activity"
msgstr "פעילות מערכת"
