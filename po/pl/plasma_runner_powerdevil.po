# translation of plasma_runner_powerdevil.po to
# translation of krunner_powerdevil.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marta Rybczyńska <kde-i18n@rybczynska.net>, 2009, 2010.
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2018, 2019, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-09 01:58+0000\n"
"PO-Revision-Date: 2023-09-17 13:04+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.3\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: PowerDevilRunner.cpp:33
#, kde-format
msgctxt "Note this is a KRunner keyword; 'power' as in 'power saving mode'"
msgid "power"
msgstr "zasilanie"

#: PowerDevilRunner.cpp:35
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "suspend"
msgstr "uśpienie"

#: PowerDevilRunner.cpp:37
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to ram"
msgstr "do pamięci"

#: PowerDevilRunner.cpp:39
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "sleep"
msgstr "uśpij"

#: PowerDevilRunner.cpp:41
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hibernate"
msgstr "hibernuj"

#: PowerDevilRunner.cpp:43
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to disk"
msgstr "na dysk"

#: PowerDevilRunner.cpp:45
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hybrid sleep"
msgstr "hybrydowe uśpienie"

#: PowerDevilRunner.cpp:47
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hybrid"
msgstr "hybryda"

#: PowerDevilRunner.cpp:49 PowerDevilRunner.cpp:51 PowerDevilRunner.cpp:76
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "dim screen"
msgstr "przyciemnij ekran"

#: PowerDevilRunner.cpp:60
#, kde-format
msgid ""
"Lists system suspend (e.g. sleep, hibernate) options and allows them to be "
"activated"
msgstr ""
"Pokazuje listę wszystkich opcji uśpienia (uśpienie, hibernacja) i pozwala je "
"aktywować"

#: PowerDevilRunner.cpp:64
#, kde-format
msgid "Suspends the system to RAM"
msgstr "Usypia system do pamięci"

#: PowerDevilRunner.cpp:68
#, kde-format
msgid "Suspends the system to disk"
msgstr "Usypia systemu na dysk"

#: PowerDevilRunner.cpp:72
#, kde-format
msgid "Sleeps now and falls back to hibernate"
msgstr "Usypia teraz, a później hibernuje się"

#: PowerDevilRunner.cpp:75
#, kde-format
msgctxt ""
"Note this is a KRunner keyword, <> is a placeholder and should be at the end"
msgid "screen brightness <percent value>"
msgstr "jasność ekranu <wartość procentowa>"

#: PowerDevilRunner.cpp:78
#, no-c-format, kde-format
msgid ""
"Lists screen brightness options or sets it to the brightness defined by the "
"search term; e.g. screen brightness 50 would dim the screen to 50% maximum "
"brightness"
msgstr ""
"Pokazuje opcje jasności ekranu lub ustawia jasność określoną wyrażeniem do "
"wyszukania; np. jasność ekranu 50 powoduje przyciemnienie ekranu do 50% "
"największej możliwej jasności."

#: PowerDevilRunner.cpp:100
#, kde-format
msgid "Set Brightness to %1%"
msgstr "Ustaw jasność na %1%"

#: PowerDevilRunner.cpp:109
#, kde-format
msgid "Dim screen totally"
msgstr "Całkowicie przyciemnij ekran"

#: PowerDevilRunner.cpp:117
#, kde-format
msgid "Dim screen by half"
msgstr "Przyciemnij ekran o połowę"

#: PowerDevilRunner.cpp:146
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Uśpij"

#: PowerDevilRunner.cpp:147
#, kde-format
msgid "Suspend to RAM"
msgstr "Uśpienie systemu do pamięci"

#: PowerDevilRunner.cpp:152
#, kde-format
msgctxt "Suspend to disk"
msgid "Hibernate"
msgstr "Hibernuj"

#: PowerDevilRunner.cpp:153
#, kde-format
msgid "Suspend to disk"
msgstr "Uśpij na dysk"

#: PowerDevilRunner.cpp:158
#, kde-format
msgctxt "Suspend to both RAM and disk"
msgid "Hybrid sleep"
msgstr "Hybrydowe uśpienie"

#: PowerDevilRunner.cpp:159
#, kde-format
msgid "Sleep now and fall back to hibernate"
msgstr "Uśpij teraz, a później zahibernuj"

#: PowerDevilRunner.cpp:237
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "screen brightness "
msgstr "jasność ekranu "

#: PowerDevilRunner.cpp:239
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "dim screen "
msgstr "przyciemnij ekran "

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "screen brightness"
#~ msgstr "jasność ekranu"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "screen brightness %1"
#~ msgstr "jasność ekranu %1"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "dim screen %1"
#~ msgstr "przyciemnij ekran %1"

#~ msgid "Suspend"
#~ msgstr "Uśpij"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "power profile"
#~ msgstr "profil zasilania"

#~ msgid "Lists all power profiles and allows them to be activated"
#~ msgstr ""
#~ "Pokazuje listę wszystkich profili zasilania i pozwala na ich aktywowanie"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "power profile %1"
#~ msgstr "profil zasilania %1"

#~ msgid "Set Profile to '%1'"
#~ msgstr "Ustaw profil \"%1\""

#~ msgid "Turn off screen"
#~ msgstr "Wyłącz ekran"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "cpu policy"
#~ msgstr "polityka procesora"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "power governor"
#~ msgstr "zarządzanie zasilaniem"

#~ msgid "Lists all power saving schemes and allows them to be activated"
#~ msgstr ""
#~ "Pokazuje listę wszystkich profili zarządzania energią i pozwala na ich "
#~ "aktywowanie"

#~ msgid ""
#~ "Lists all CPU frequency scaling policies and allows them to be activated"
#~ msgstr ""
#~ "Pokazuje wszystkie polityki skalowania częstotliwości procesora i pozwala "
#~ "je aktywować"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "cpu policy %1"
#~ msgstr "polityka procesora %1"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "power governor %1"
#~ msgstr "zarządzanie zasilaniem %1"

#~ msgid "Set CPU frequency scaling policy to '%1'"
#~ msgstr "Ustaw politykę skalowania częstotliwości procesora na \"%1\""

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "power scheme %1"
#~ msgstr "schemat zasilania %1"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "power scheme"
#~ msgstr "schemat zasilania"

#~ msgid "Set Powersaving Scheme to '%1'"
#~ msgstr "Ustaw schemat zarządzania zasilaniem \"%1\""

#~ msgid "Set CPU Governor to '%1'"
#~ msgstr "Ustaw sposób zarządzania procesorem \"%1\""
