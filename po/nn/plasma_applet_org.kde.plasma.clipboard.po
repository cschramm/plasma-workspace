# Translation of plasma_applet_org.kde.plasma.clipboard to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2018, 2019, 2020, 2021, 2023.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-16 01:40+0000\n"
"PO-Revision-Date: 2023-05-27 14:13+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr "QR-kode"

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr "Datamatrise"

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr "Kode 39"

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr "Kode 93"

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr "Kode 128"

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr "Tilbake til utklippstavla"

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr "Endra type QR-kode"

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr "Klarte ikkje laga QR-kode"

#: contents/ui/BarcodePage.qml:147
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr "Det er ikkje plass til å visa QR-koden. Prøv å gjera vindauget større."

#: contents/ui/DelegateToolButtons.qml:23
#, kde-format
msgid "Invoke action"
msgstr "Utfør handling"

#: contents/ui/DelegateToolButtons.qml:38
#, kde-format
msgid "Show QR code"
msgstr "Vis QR-kode"

#: contents/ui/DelegateToolButtons.qml:54
#, kde-format
msgid "Edit contents"
msgstr "Rediger innhald"

#: contents/ui/DelegateToolButtons.qml:68
#, kde-format
msgid "Remove from history"
msgstr "Fjern frå loggen"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Lagra"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Avbryt"

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Innhald på utklippstavla"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:129
#, kde-format
msgid "Clipboard is empty"
msgstr "Utklippstavla er tom"

#: contents/ui/main.qml:59
#, kde-format
msgid "Clear History"
msgstr "Tøm loggen"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr "Set opp utklippstavla …"

#: contents/ui/Menu.qml:129
#, kde-format
msgid "No matches"
msgstr "Ingen treff"

#: contents/ui/UrlItemDelegate.qml:106
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"
